import { Component, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  purchases : number[] = [];
  total = 0;
  payments : number[] = [];
  paymentSum = 0;
  remain = 0;
  remainChanges = [0,0,0]
  change = 0;
  formPayment = new FormGroup({
    payment: new FormControl(0,[Validators.required,Validators.min(0)])
  });
  form = new FormGroup({
    purchase: new FormControl(0,[Validators.required,Validators.min(0)])
  });
  ngOnInit() {
 
  }
  purchase(){
    const newPurchase =  this.form.controls['purchase'].value;
    this.purchases.push(newPurchase)
    this.total = this.total + newPurchase;
    this.form.controls['purchase'].setValue(0);
    this.remain = this.total - this.paymentSum;
    this.change = this.paymentSum - this.total ;
    if(this.change > 0){
      this.calculateChanges();
    }
    
 

  }
  onSubmitPayment(){
    const newPayment =  this.formPayment.controls['payment'].value;
    this.paymentSum = this.paymentSum + newPayment
    this.payments.push(newPayment)
    this.formPayment.controls['payment'].setValue(0)
    this.remain = this.total - this.paymentSum;
    this.change = this.paymentSum - this.total ;
    if(this.change > 0){
      this.calculateChanges();
    }

  }

  calculateChanges(){
    this.remainChanges[0] = (this.change -this.change % 20) / 20;
    let remainsChange = this.change - (this.change -this.change % 20);
    this.remainChanges[1] = ( remainsChange - this.change % 10) / 10;
    remainsChange = remainsChange % 10 ;
    this.remainChanges[2] =  remainsChange;
  } 
}
